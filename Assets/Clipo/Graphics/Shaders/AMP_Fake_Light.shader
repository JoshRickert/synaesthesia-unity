// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Clipo/Unlit_FakeLight"
{
	Properties
	{
		[Toggle(_USE_ARRAY_ON)] _Use_Array("Use_Array", Float) = 0
		_Albedo_Array("Albedo_Array", 2DArray ) = "" {}
		_ArrayIndex("Array Index", Float) = 0
		_Albedo("Albedo", 2D) = "white" {}
		_Nomral("Nomral", 2D) = "bump" {}
		_lightpos("light pos", Vector) = (3.32,5,-2.13,0)
		_LightPwr("LightPwr", Range( 0 , 3)) = 0.5
		_Shadows("Shadows", Float) = 0.1
		_LightBouncedcolor("Light Bounced color", Color) = (0.07058824,0.09019608,0.1215686,0)
		[Toggle(_LIGHTBOUNCE_ON)] _LightBounce("Light Bounce", Float) = 1
		[Toggle(_EMISSION_ON)] _Emission("Emission", Float) = 0
		[HDR]_EmissionfromAlbedo_Clamp("EmissionfromAlbedo_Clamp", Range( 0 , 1)) = 0.2
		_EmissionMultiplier("Emission Multiplier", Float) = 1
		[Toggle(_EMISSION_ONLY_R_ON)] _Emission_only_R("Emission_only_R", Float) = 0
		_Clipping("Clipping", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		
		Tags { "RenderPipeline"="UniversalPipeline" "RenderType"="Opaque" "Queue"="Geometry" }
		
		Cull Back
		HLSLINCLUDE
		#pragma target 2.0
		ENDHLSL

		
		Pass
		{
			Name "Forward"
			Tags { "LightMode"="UniversalForward" }
			
			Blend One Zero , One Zero
			ZWrite On
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA
			

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#define ASE_SRP_VERSION 999999
			#define _AlphaClip 1

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag


			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

			#pragma shader_feature _EMISSION_ON
			#pragma shader_feature _LIGHTBOUNCE_ON
			#pragma shader_feature _USE_ARRAY_ON
			#pragma shader_feature _EMISSION_ONLY_R_ON


			sampler2D _Nomral;
			sampler2D _Albedo;
			TEXTURE2D_ARRAY( _Albedo_Array );
			uniform SAMPLER( sampler_Albedo_Array );
			CBUFFER_START( UnityPerMaterial )
			float _LightPwr;
			float3 _lightpos;
			float4 _Nomral_ST;
			float _Shadows;
			float4 _Albedo_ST;
			float4 _Albedo_Array_ST;
			float _ArrayIndex;
			float4 _LightBouncedcolor;
			float _EmissionfromAlbedo_Clamp;
			float _EmissionMultiplier;
			float _Clipping;
			CBUFFER_END


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_tangent : TANGENT;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#ifdef ASE_FOG
				float fogFactor : TEXCOORD0;
				#endif
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			
			VertexOutput vert ( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord1.xyz = ase_worldTangent;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord2.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord3.xyz = ase_worldBitangent;
				
				o.ase_texcoord4.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.w = 0;
				o.ase_texcoord2.w = 0;
				o.ase_texcoord3.w = 0;
				o.ase_texcoord4.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif
				v.ase_normal = v.ase_normal;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex.xyz);
				o.clipPos = vertexInput.positionCS;
				#ifdef ASE_FOG
				o.fogFactor = ComputeFogFactor( vertexInput.positionCS.z );
				#endif
				return o;
			}

			half4 frag ( VertexOutput IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float3 normalizeResult14 = normalize( _lightpos );
				float3 ase_worldTangent = IN.ase_texcoord1.xyz;
				float3 ase_worldNormal = IN.ase_texcoord2.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord3.xyz;
				float3x3 ase_worldToTangent = float3x3(ase_worldTangent,ase_worldBitangent,ase_worldNormal);
				float3 worldToTangentDir7 = mul( ase_worldToTangent, normalizeResult14);
				float2 uv_Nomral = IN.ase_texcoord4.xy * _Nomral_ST.xy + _Nomral_ST.zw;
				float3 tex2DNode18 = UnpackNormalScale( tex2D( _Nomral, uv_Nomral ), 1.0f );
				float dotResult17 = dot( worldToTangentDir7 , tex2DNode18 );
				float2 uv_Albedo = IN.ase_texcoord4.xy * _Albedo_ST.xy + _Albedo_ST.zw;
				float2 uv_Albedo_Array = IN.ase_texcoord4.xy * _Albedo_Array_ST.xy + _Albedo_Array_ST.zw;
				float4 texArray61 = SAMPLE_TEXTURE2D_ARRAY(_Albedo_Array, sampler_Albedo_Array, uv_Albedo_Array, _ArrayIndex );
				#ifdef _USE_ARRAY_ON
				float4 staticSwitch59 = texArray61;
				#else
				float4 staticSwitch59 = tex2D( _Albedo, uv_Albedo );
				#endif
				float4 temp_output_13_0 = ( ( _LightPwr * (_Shadows + (max( dotResult17 , 0.0 ) - 0.0) * (1.0 - _Shadows) / (1.0 - 0.0)) ) * staticSwitch59 );
				float3 normalizeResult21 = normalize( float3(0,-10,0) );
				float3 worldToTangentDir23 = mul( ase_worldToTangent, normalizeResult21);
				float dotResult22 = dot( worldToTangentDir23 , tex2DNode18 );
				#ifdef _LIGHTBOUNCE_ON
				float4 staticSwitch29 = ( temp_output_13_0 + ( max( dotResult22 , 0.0 ) * _LightBouncedcolor ) );
				#else
				float4 staticSwitch29 = temp_output_13_0;
				#endif
				float4 Albedo34 = staticSwitch59;
				float4 break48 = Albedo34;
				#ifdef _EMISSION_ONLY_R_ON
				float staticSwitch51 = break48.r;
				#else
				float staticSwitch51 = ( break48.r + break48.g + break48.b );
				#endif
				float clampResult50 = clamp( staticSwitch51 , 0.0 , 1.0 );
				float clampResult55 = clamp( ( (0.0 + (clampResult50 - _EmissionfromAlbedo_Clamp) * (1.0 - 0.0) / (1.0 - _EmissionfromAlbedo_Clamp)) * _EmissionMultiplier ) , 1.0 , 20.0 );
				#ifdef _EMISSION_ON
				float4 staticSwitch32 = ( staticSwitch29 * clampResult55 );
				#else
				float4 staticSwitch32 = staticSwitch29;
				#endif
				
				float3 BakedAlbedo = 0;
				float3 BakedEmission = 0;
				float3 Color = staticSwitch32.rgb;
				float Alpha = Albedo34.a;
				float AlphaClipThreshold = _Clipping;

				#if _AlphaClip
					clip( Alpha - AlphaClipThreshold );
				#endif

				#ifdef ASE_FOG
					Color = MixFog( Color, IN.fogFactor );
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				return half4( Color, Alpha );
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "ShadowCaster"
			Tags { "LightMode"="ShadowCaster" }

			ZWrite On
			ZTest LEqual

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#define ASE_SRP_VERSION 999999
			#define _AlphaClip 1

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment


			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#pragma shader_feature _USE_ARRAY_ON


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			sampler2D _Albedo;
			TEXTURE2D_ARRAY( _Albedo_Array );
			uniform SAMPLER( sampler_Albedo_Array );
			CBUFFER_START( UnityPerMaterial )
			float _LightPwr;
			float3 _lightpos;
			float4 _Nomral_ST;
			float _Shadows;
			float4 _Albedo_ST;
			float4 _Albedo_Array_ST;
			float _ArrayIndex;
			float4 _LightBouncedcolor;
			float _EmissionfromAlbedo_Clamp;
			float _EmissionMultiplier;
			float _Clipping;
			CBUFFER_END


			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				float4 ase_texcoord7 : TEXCOORD7;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			
			float3 _LightDirection;

			VertexOutput ShadowPassVertex( VertexInput v )
			{
				VertexOutput o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );

				o.ase_texcoord7.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord7.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld(v.vertex.xyz);
				float3 normalWS = TransformObjectToWorldDir(v.ase_normal);

				float4 clipPos = TransformWorldToHClip( ApplyShadowBias( positionWS, normalWS, _LightDirection ) );

				#if UNITY_REVERSED_Z
					clipPos.z = min(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#else
					clipPos.z = max(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#endif
				o.clipPos = clipPos;

				return o;
			}

			half4 ShadowPassFragment(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 uv_Albedo = IN.ase_texcoord7.xy * _Albedo_ST.xy + _Albedo_ST.zw;
				float2 uv_Albedo_Array = IN.ase_texcoord7.xy * _Albedo_Array_ST.xy + _Albedo_Array_ST.zw;
				float4 texArray61 = SAMPLE_TEXTURE2D_ARRAY(_Albedo_Array, sampler_Albedo_Array, uv_Albedo_Array, _ArrayIndex );
				#ifdef _USE_ARRAY_ON
				float4 staticSwitch59 = texArray61;
				#else
				float4 staticSwitch59 = tex2D( _Albedo, uv_Albedo );
				#endif
				float4 Albedo34 = staticSwitch59;
				
				float Alpha = Albedo34.a;
				float AlphaClipThreshold = _Clipping;

				#if _AlphaClip
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "DepthOnly"
			Tags { "LightMode"="DepthOnly" }

			ZWrite On
			ColorMask 0

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#define ASE_SRP_VERSION 999999
			#define _AlphaClip 1

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag


			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#pragma shader_feature _USE_ARRAY_ON


			sampler2D _Albedo;
			TEXTURE2D_ARRAY( _Albedo_Array );
			uniform SAMPLER( sampler_Albedo_Array );
			CBUFFER_START( UnityPerMaterial )
			float _LightPwr;
			float3 _lightpos;
			float4 _Nomral_ST;
			float _Shadows;
			float4 _Albedo_ST;
			float4 _Albedo_Array_ST;
			float _ArrayIndex;
			float4 _LightBouncedcolor;
			float _EmissionfromAlbedo_Clamp;
			float _EmissionMultiplier;
			float _Clipping;
			CBUFFER_END


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			
			VertexOutput vert( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				o.ase_texcoord.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				o.clipPos = TransformObjectToHClip(v.vertex.xyz);
				return o;
			}

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 uv_Albedo = IN.ase_texcoord.xy * _Albedo_ST.xy + _Albedo_ST.zw;
				float2 uv_Albedo_Array = IN.ase_texcoord.xy * _Albedo_Array_ST.xy + _Albedo_Array_ST.zw;
				float4 texArray61 = SAMPLE_TEXTURE2D_ARRAY(_Albedo_Array, sampler_Albedo_Array, uv_Albedo_Array, _ArrayIndex );
				#ifdef _USE_ARRAY_ON
				float4 staticSwitch59 = texArray61;
				#else
				float4 staticSwitch59 = tex2D( _Albedo, uv_Albedo );
				#endif
				float4 Albedo34 = staticSwitch59;
				
				float Alpha = Albedo34.a;
				float AlphaClipThreshold = _Clipping;

				#if _AlphaClip
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}
			ENDHLSL
		}

	
	}
	CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
	Fallback "Hidden/InternalErrorShader"
	
}
/*ASEBEGIN
Version=17200
2087;114;1583;767;3577.214;1451.618;4.074466;True;True
Node;AmplifyShaderEditor.CommentaryNode;4;451.8984,-391.718;Inherit;False;1040.965;576.037;Albedo + Light;8;19;13;12;10;34;59;61;65;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;65;479.8339,17.65216;Inherit;False;Property;_ArrayIndex;Array Index;2;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureArrayNode;61;665.9358,-28.47584;Inherit;True;Property;_Albedo_Array;Albedo_Array;1;0;Create;True;0;0;False;0;None;0;Object;-1;Auto;False;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;19;479.2714,-222.7327;Inherit;True;Property;_Albedo;Albedo;3;0;Create;True;0;0;False;0;-1;None;124fbb1dfac24459aabdc88cda07fbc3;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;59;1011.973,-87.53597;Inherit;False;Property;_Use_Array;Use_Array;0;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;34;1265.564,-89.81329;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;31;-813.6912,248.6814;Inherit;False;1491.384;300.0933;Fake light bounce;7;24;22;20;23;21;25;27;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;5;-1535.436,-223.2779;Inherit;False;1317.517;344.9882;Fake Light directional;5;17;16;14;8;7;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;56;2060.416,557.9474;Inherit;False;34;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;6;-167.218,-215.8862;Inherit;False;581.8972;411.1597;Shadow Boost;5;15;11;3;2;1;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;42;817.5814,917.6896;Inherit;False;Property;_EmissionMultiplier;Emission Multiplier;12;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;1126.791,642.4937;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;32;2331.132,150.0517;Inherit;False;Property;_Emission;Emission;10;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;55;1782.173,539.8487;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;20;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;57;2296.846,556.9276;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleAddOpNode;28;1570.36,30.51198;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;58;2262.541,733.6861;Inherit;False;Property;_Clipping;Clipping;14;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;21;-605.5905,295.7148;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;64;1466.885,272.5675;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;1298.363,-246.7602;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;12;781.4901,-357.2249;Inherit;False;Property;_LightPwr;LightPwr;6;0;Create;True;0;0;False;0;0.5;3;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;63;482.3458,-262.0177;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;14;-1338.883,-165.6822;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;29;1769.639,-75.81517;Inherit;False;Property;_LightBounce;Light Bounce;9;0;Create;True;0;0;False;0;0;1;1;True;;Toggle;2;Key0;Key1;Create;False;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DotProductOpNode;22;-196.1087,294.1846;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;46;2003.358,293.8602;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;1099.467,-346.9349;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TransformDirectionNode;7;-1184.083,-169.0823;Inherit;False;World;Tangent;False;Fast;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ColorNode;27;256.0135,365.7964;Inherit;False;Property;_LightBouncedcolor;Light Bounced color;8;0;Create;True;0;0;False;0;0.07058824,0.09019608,0.1215686,0;0.1423104,0.1819152,0.2452829,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;38;757.6478,645.2966;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;48;-642.5154,599.1621;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.TFHCRemapNode;11;75.39657,-171.8559;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;52;-314.0091,780.0182;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;51;48.97478,741.4925;Inherit;False;Property;_Emission_only_R;Emission_only_R;13;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;24;-781.8154,291.6259;Inherit;False;Constant;_Vector0;Vector 0;2;0;Create;True;0;0;False;0;0,-10,0;3.32,5,-2.13;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;15;-119.1069,14.75967;Inherit;False;Property;_Shadows;Shadows;7;0;Create;True;0;0;False;0;0.1;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;20;30.06224,296.7978;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TransformDirectionNode;23;-450.7902,292.3147;Inherit;False;World;Tangent;False;Fast;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;17;-921.4,-165.2124;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;18;-1302.312,-471.1955;Inherit;True;Property;_Nomral;Nomral;4;0;Create;True;0;0;False;0;-1;None;ac1f3b869524e4fef930635ca917a1dc;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;35;-841.98,593.1877;Inherit;False;34;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;50;409.439,642.916;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;530.2491,288.6399;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.Vector3Node;16;-1515.108,-169.7711;Inherit;False;Property;_lightpos;light pos;5;0;Create;True;0;0;False;0;3.32,5,-2.13;3.32,5,-2.13;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;49;-184.1611,642.3162;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;8;-703.229,-164.5992;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;39;429.0586,793.2861;Inherit;False;Property;_EmissionfromAlbedo_Clamp;EmissionfromAlbedo_Clamp;11;1;[HDR];Create;True;0;0;False;0;0.2;0.2;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;2;0,0;Float;False;False;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;5;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;DepthOnly;0;2;DepthOnly;0;False;False;False;True;0;False;-1;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;True;False;False;False;False;0;False;-1;False;True;1;False;-1;False;False;True;1;LightMode=DepthOnly;False;0;Hidden/InternalErrorShader;0;0;Standard;0;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1;0,0;Float;False;False;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;5;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ShadowCaster;0;1;ShadowCaster;0;False;False;False;True;0;False;-1;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;False;True;1;LightMode=ShadowCaster;False;0;Hidden/InternalErrorShader;0;0;Standard;0;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;2710.792,285.9502;Float;False;True;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;5;Clipo/Unlit_FakeLight;2992e84f91cbeb14eab234972e07ea9d;True;Forward;0;0;Forward;7;False;False;False;True;0;False;-1;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;True;1;1;False;-1;0;False;-1;1;1;False;-1;0;False;-1;False;False;False;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=UniversalForward;False;0;Hidden/InternalErrorShader;0;0;Standard;10;Surface;0;  Blend;0;Two Sided;1;Cast Shadows;1;Receive Shadows;1;GPU Instancing;1;LOD CrossFade;0;Built-in Fog;0;Meta Pass;0;Vertex Position,InvertActionOnDeselection;1;0;4;True;True;True;False;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;3;0,0;Float;False;False;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;5;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;Meta;0;3;Meta;0;False;False;False;True;0;False;-1;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;True;2;False;-1;False;False;False;False;False;True;1;LightMode=Meta;False;0;Hidden/InternalErrorShader;0;0;Standard;0;0
WireConnection;61;1;65;0
WireConnection;59;1;19;0
WireConnection;59;0;61;0
WireConnection;34;0;59;0
WireConnection;41;0;38;0
WireConnection;41;1;42;0
WireConnection;32;1;29;0
WireConnection;32;0;46;0
WireConnection;55;0;41;0
WireConnection;57;0;56;0
WireConnection;28;0;13;0
WireConnection;28;1;64;0
WireConnection;21;0;24;0
WireConnection;64;0;25;0
WireConnection;13;0;10;0
WireConnection;13;1;59;0
WireConnection;63;0;11;0
WireConnection;14;0;16;0
WireConnection;29;1;13;0
WireConnection;29;0;28;0
WireConnection;22;0;23;0
WireConnection;22;1;18;0
WireConnection;46;0;29;0
WireConnection;46;1;55;0
WireConnection;10;0;12;0
WireConnection;10;1;63;0
WireConnection;7;0;14;0
WireConnection;38;0;50;0
WireConnection;38;1;39;0
WireConnection;48;0;35;0
WireConnection;11;0;8;0
WireConnection;11;3;15;0
WireConnection;52;0;48;0
WireConnection;51;1;49;0
WireConnection;51;0;52;0
WireConnection;20;0;22;0
WireConnection;23;0;21;0
WireConnection;17;0;7;0
WireConnection;17;1;18;0
WireConnection;50;0;51;0
WireConnection;25;0;20;0
WireConnection;25;1;27;0
WireConnection;49;0;48;0
WireConnection;49;1;48;1
WireConnection;49;2;48;2
WireConnection;8;0;17;0
WireConnection;0;2;32;0
WireConnection;0;3;57;3
WireConnection;0;4;58;0
ASEEND*/
//CHKSM=65BEA99413164E30865C23E1CD6D321068DA4188