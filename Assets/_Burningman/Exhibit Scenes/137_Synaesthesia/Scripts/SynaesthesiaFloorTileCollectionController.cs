﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SynaesthesiaCamp_137
{
    public class SynaesthesiaFloorTileCollectionController : MonoBehaviour
    {
        [SerializeField]
        SynaesthesiaFloorTile[] _floorTiles;
        int _floorTilesTotal;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            _floorTilesTotal = _floorTiles.Length;
        }

        // Update is called once per frame
        void Update()
        {
            for (int i = 0; i < _floorTilesTotal; i++) {
                var floorTile = _floorTiles[i];
                // float red = (Time.time / 5) % 1;
                float red = (Mathf.Sin((Time.time+i)*4) + 1)/2;
                // float green = 1.4f;
                float green = (Mathf.Sin((Time.time - i) * 6) + 1);
                // float blue = (Mathf.Cos((Time.time) / 3) + 1);
                float blue = 1.4f;
                Color newColor = new Color(red, green, blue);
                floorTile.SetColor(newColor);
            }
        }
    }
}