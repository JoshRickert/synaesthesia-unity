using UnityEngine;

namespace SynaesthesiaCamp_137 {
    public class SynaesthesiaFloorTile : MonoBehaviour
    {

        static string EMISSION_COLOR_KEY = "_EmissionColor";
        public Renderer ModelRenderer;
        public Color OriginalColor = new Color(1.4f, 1.4f, 1.4f);
        public Color StepOnColor = new Color(1.4f, 1.4f, 0f);
        public bool IsSteppedOn { get; private set; }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        void Start()
        {
            ModelRenderer.material.SetColor(EMISSION_COLOR_KEY, OriginalColor);
        }

        public void StepOn()
        {
            IsSteppedOn = true;
            ModelRenderer.material.SetColor(EMISSION_COLOR_KEY, StepOnColor);
        }

        public void StepOff()
        {
            IsSteppedOn = false;
            ModelRenderer.material.SetColor(EMISSION_COLOR_KEY, OriginalColor);
        }

        public void SetColor(Color newColor) {
            OriginalColor = newColor;
            if(!IsSteppedOn) {
                ModelRenderer.material.SetColor(EMISSION_COLOR_KEY, OriginalColor);
            }
        }

        /// <summary>
        /// OnTriggerEnter is called when the Collider other enters the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        void OnTriggerEnter(Collider other)
        {
            StepOn();
        }

        /// <summary>
        /// OnTriggerExit is called when the Collider other has stopped touching the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        void OnTriggerExit(Collider other)
        {
            StepOff();
        }
    }
}