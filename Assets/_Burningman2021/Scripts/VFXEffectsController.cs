﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clipo {
  public class VFXEffectsController : MonoBehaviour {
    enum Group {
      Group1,
      Group2,
      Group3,
      Group4,
      Group5,
      Group6,
      Group7,
      Group8,
    }

    [SerializeField]
    Group effectGroup;

    [SerializeField]
    bool activeOnStart = false;
  }
}