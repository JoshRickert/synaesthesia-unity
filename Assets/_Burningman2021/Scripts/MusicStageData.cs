using Newtonsoft.Json;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// To access BPM:
/// - Add this script to scene
/// - set stage id. For synaesthesia use the stage ID emailed by Jacek to Josh and Syn team
/// - from your script access: MusicStageData.StageData.bpm
/// - for testing set to true in Editor "useEditorStageData", then you can simulate Stage data
/// </summary>
public class MusicStageData : MonoBehaviour {
  [System.Serializable]
  public class Stage {
    public string id;
    public float bpm;
  }
  private class GetStageResponse {
    public Stage Stage { get; set; }
  }

  [SerializeField]
  private string stageId = "XXXXXXXXXXXXXXXXXXXXXX";
  public Stage StageData { get; private set; }

#if UNITY_EDITOR
  [SerializeField]
  private bool useEditorStageData = false;
  [SerializeField]
  private Stage editorStageData = default;
#endif

  private readonly string
      getStageEndpoint = "https://api.dustymultiverseapp.com/stages/{0}" +
        "?stageFields[]=id" +
        "&stageFields[]=bpm" +
        "&currentlyPlayedFields[]=null";

  private float
    lastRefreshTime = 0f,
    refreshTime = 1f;

  private void Update() {
    if (Time.time - lastRefreshTime > refreshTime) {

      lastRefreshTime = Time.time;

#if UNITY_EDITOR
      if (useEditorStageData) {
        StageDataChanged(JsonConvert.DeserializeObject<Stage>(JsonConvert.SerializeObject(editorStageData)));
      }
      else
#endif
      {
        RefreshStageData();
      }
    }
  }

  private void RefreshStageData() {
    if (!string.IsNullOrEmpty(stageId)) {
      var url = string.Format(getStageEndpoint, stageId);
      StartCoroutine(GetRequestCoroutine(url));
    }
  }
  private void StageDataChanged(Stage newStageData) {
    StageData = newStageData;
  }

  private void HandleGetStageResponse(string json) {
    if (!string.IsNullOrEmpty(json)) {
      var parsedData = JsonConvert.DeserializeObject<GetStageResponse>(json);
      if (parsedData != null) {
        StageDataChanged(parsedData.Stage);

#if UNITY_EDITOR
        if (!useEditorStageData) {
          editorStageData = parsedData.Stage;
        }
#endif
      }
    }
  }

  private IEnumerator GetRequestCoroutine(string url) {
    using (var webRequest = UnityWebRequest.Get(url)) {
      yield return webRequest.SendWebRequest();

      if (webRequest.isNetworkError || webRequest.isHttpError) {
        Debug.Log("[GetStage] Error: " + webRequest.error);
      }
      else {
        HandleGetStageResponse(webRequest.downloadHandler.text);
      }
    }
  }
}
