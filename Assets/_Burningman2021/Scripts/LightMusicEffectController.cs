﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightMusicEffectController : MonoBehaviour
{
    [SerializeField]
    AudioMeasureCS audioData;
    [SerializeField]
    GameObject targetParticleSystem;

    ParticleSystem.NoiseModule noise;
    ParticleSystem.MainModule main;

    [Range(0.001f, 15f)]
    public float multiplier = 1f;
    public EffectInfluenceType influenceType;

    void Start()
    {
      noise = targetParticleSystem.GetComponent<ParticleSystem>().noise;
      main = targetParticleSystem.GetComponent<ParticleSystem>().main;
    }

    void Update()
    {
      switch(influenceType)
      {
        case EffectInfluenceType.NoiseFrequency:
          ChangeNoiseFrequency();
        break;
        case EffectInfluenceType.NoisePositionAmount:
          ChangeNoisePosAmount();
        break;
        case EffectInfluenceType.SimulationSpeed:
          ChangeSimulationSpeed();
        break;
	case EffectInfluenceType.NoiseStrenght:
          ChangeNoiseStrenght();
        break;

      }
    }

    void ChangeNoiseStrenght() {
      noise.strength = audioData.RmsValue * multiplier;
    }

    void ChangeSimulationSpeed() {
      main.simulationSpeed = audioData.RmsValue * multiplier;
    }

    void ChangeNoisePosAmount() {
      noise.positionAmount = audioData.RmsValue * multiplier;
    }

    void ChangeNoiseFrequency() {
      noise.frequency = audioData.RmsValue * multiplier;
    }
}

public enum EffectInfluenceType {
  SimulationSpeed = 0,
  NoisePositionAmount = 1,
  NoiseFrequency = 2,
  NoiseStrenght = 3
}
