﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Modified version of LightMusicEffectController with additional features.
/// </summary>
public class SynaesthesiaLightMusicEffectController : MonoBehaviour
{
    [SerializeField]
    SynaesthesiaAudioMeasureCS audioData;
    /// <summary>Changed from GameObject in original class to direct ParticleSystem reference</summary>
    [SerializeField]
    ParticleSystem targetParticleSystem;
    ParticleSystem.Particle[] particles;

    ParticleSystem.NoiseModule noise;
    ParticleSystem.MainModule main;

    [Header("Color Change Settings")]
    [SerializeField]
    [Tooltip("Gradient which will change the colors of particles according to the incoming PitchValue")]
    Gradient colorChangeSpectrum;
    [Range(20f, 20000f)]
    [Tooltip("Hz at which the particles will be the color on the far left of the gradient")]
    public float minFrequency = 20f;
    [Range(20f, 20000f)]
    [Tooltip("Hz at which the particles will be the color on the far right of the gradient")]
    public float maxFrequency = 20000f;

    [Header("Global settings")]
    [Range(0.001f, 15f)]
    public float multiplier = 1f;
    public SynaesthesiaEffectInfluenceType influenceType;
    public int updateEveryXFrames = 1;

    void Start()
    {
        noise = targetParticleSystem.noise;
        main = targetParticleSystem.main;

        particles = new ParticleSystem.Particle[targetParticleSystem.main.maxParticles];
    }

    void Update()
    {
        if (Time.frameCount % updateEveryXFrames != 0)
        {
            return;
        }

        switch (influenceType)
        {
            case SynaesthesiaEffectInfluenceType.NoiseFrequency:
                ChangeNoiseFrequency();
                break;
            case SynaesthesiaEffectInfluenceType.NoisePositionAmount:
                ChangeNoisePosAmount();
                break;
            case SynaesthesiaEffectInfluenceType.SimulationSpeed:
                ChangeSimulationSpeed();
                break;
            case SynaesthesiaEffectInfluenceType.NoiseStrength:
                ChangeNoiseStrength();
                break;
            case SynaesthesiaEffectInfluenceType.ColorChange:
                ChangeColor();
                break;
            case SynaesthesiaEffectInfluenceType.SizeChange:
                ChangeSize();
                break;
        }
    }

    void ChangeNoiseStrength()
    {
        noise.strength = audioData.RmsValue * multiplier;
    }

    void ChangeSimulationSpeed()
    {
        main.simulationSpeed = audioData.RmsValue * multiplier;
    }

    void ChangeNoisePosAmount()
    {
        noise.positionAmount = audioData.RmsValue * multiplier;
    }

    void ChangeNoiseFrequency()
    {
        noise.frequency = audioData.RmsValue * multiplier;
    }

    void ChangeColor()
    {
        // Calculate color
        Color newColor = colorChangeSpectrum.Evaluate(Mathf.Pow(Mathf.InverseLerp(minFrequency, maxFrequency, audioData.PitchValue), multiplier));

        // Update start color for new particles
        main.startColor = newColor;

        // Update color for existing particles
        int numParticlesAlive = targetParticleSystem.GetParticles(particles);

        // Change only the particles that are alive
        for (int i = 0; i < numParticlesAlive; i++)
        {
            particles[i].startColor = newColor;
        }

        // Apply the particle changes to the Particle System
        targetParticleSystem.SetParticles(particles, numParticlesAlive);
    }

    void ChangeSize()
    {
        // Calculate size
        float newSize = audioData.RmsValue * multiplier;

        // Update start size for new particles
        main.startSize = newSize;

        // Update size for existing particles
        int numParticlesAlive = targetParticleSystem.GetParticles(particles);

        // Change only the particles that are alive
        for (int i = 0; i < numParticlesAlive; i++)
        {
            particles[i].startSize = newSize;
        }

        // Apply the particle changes to the Particle System
        targetParticleSystem.SetParticles(particles, numParticlesAlive);
    }
}

public enum SynaesthesiaEffectInfluenceType
{
    SimulationSpeed,
    NoisePositionAmount,
    NoiseFrequency,
    NoiseStrength,
    ColorChange,
    SizeChange
}
